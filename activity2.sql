--Get all songs from the album "24K Magic" that are of the genre "Funk"
SELECT * FROM songs
JOIN albums ON songs.album_id = albums.id
WHERE albums.album_title = "24K Magic" AND songs.genre LIKE "%Funk%";

--Get the album title and artist name for all songs with a length less than 3 minutes that were release after 2015
SELECT albums.album_title, artists.name
FROM songs
JOIN albums ON songs.album_id = albums.id
JOIN artists ON albums.artist_id = artists.id
WHERE songs.length < "00:03:00" AND albums.date_released > "2015-01-01";


-- Get all artists and their albums where the artist name ends with the letter "o" and the album was release before 2013
SELECT artists.name, albums.album_title
FROM artists
JOIN albums ON artists.id = albums.artist_id
WHERE artists.name LIKE "%o" AND albums.date_released < "2013-01-01";

-- Get all songs with a length between 3 and 4 minutes from albums release by Taylor Swift
SELECT song_name
FROM songs
JOIN albums ON songs.album_id = albums.id
JOIN artists ON albums.artist_id = artists.id
WHERE artists.name = "Taylor Swift" AND songs.length > "00:03:00" AND songs.length < "00:04:00";


-- Get all albums and their release dates for which the album title contains the word "born" and the artist name starts with the letter "J"
SELECT album_title, date_released
FROM albums
JOIN artists ON albums.artist_id = artists.id
WHERE albums.album_title LIKE "%born%" AND artists.name LIKE "J%";


-- Get the names of all songs by artists whose name starts with "B"
SELECT song_name
FROM songs
JOIN albums ON songs.album_id = albums.id
JOIN artists ON albums.artist_id = artists.id
WHERE artists.name LIKE "B%";


-- Get the names of all songs longer than 4 minutes
SELECT song_name FROM songs WHERE length > "00:04:00";
